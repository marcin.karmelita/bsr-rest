package bds.rest.exceptions

class NotFoundException : Exception()