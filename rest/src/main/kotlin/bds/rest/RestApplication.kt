package bds.rest

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories

@EnableMongoRepositories
@SpringBootApplication
class RestApplication

fun main(args: Array<String>) {
    SpringApplication.run(RestApplication::class.java, *args)
}
