package bds.rest.db

import bds.rest.models.Transaction
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Component

@Component
@Document(collection = "transactions")
interface TransactionsRepository: MongoRepository<Transaction, String> {
    fun findBySourceAccount(sourceAccount: String): List<Transaction>
}
