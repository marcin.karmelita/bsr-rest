package bds.rest.models

import com.fasterxml.jackson.annotation.JsonProperty
import org.jetbrains.annotations.Nullable
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull
import javax.validation.constraints.Pattern
import javax.xml.bind.annotation.*

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
class Transaction {

    companion object {
        const val accountNumberRegex: String = "^[0-9]{26}\$"
    }

    @XmlElement(name = "source_account")
    @JsonProperty("source_account")
    @NotNull
    @Pattern(regexp = Transaction.accountNumberRegex)
    var sourceAccount: String? = null

    @XmlElement(name = "source_name")
    @JsonProperty("source_name")
    @NotNull
    var sourceName: String? = null

    @Min(1)
    @Max(10000000000L)
    var amount: Int = 0

    @NotNull
    var title: String? = null

    @XmlElement(name = "destination_name")
    @JsonProperty("destination_name")
    @NotNull
    var destinationName: String? = null

    @XmlTransient
    internal var balance: Int = 0

//    @XmlTransient
    @XmlElement(name = "destination_account")
    @JsonProperty("destination_account")
    @Nullable
    @Pattern(regexp = Transaction.accountNumberRegex)
    var destinationAccount: String? = null
}
