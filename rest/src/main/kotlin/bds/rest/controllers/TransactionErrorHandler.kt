package bds.rest.controllers

import bds.rest.exceptions.BadRequestException
import bds.rest.exceptions.NotFoundException
import bds.rest.models.ErrorModel
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.servlet.mvc.support.DefaultHandlerExceptionResolver


@ControllerAdvice
class TransactionErrorHandler : DefaultHandlerExceptionResolver() {

    @ExceptionHandler(NotFoundException::class)
    fun handleNotFound(exception: NotFoundException): ResponseEntity<Any> {
        return ResponseEntity.notFound().build()
    }

    @ExceptionHandler(BadRequestException::class)
    fun handleBadRequest(exception: BadRequestException): ResponseEntity<List<ErrorModel>> {
        return ResponseEntity.badRequest().body(exception.errorModels)
    }
}
