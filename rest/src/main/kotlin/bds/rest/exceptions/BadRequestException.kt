package bds.rest.exceptions

import bds.rest.models.ErrorModel
import org.springframework.validation.FieldError

class BadRequestException(val errorModels: List<ErrorModel>): Exception() {

    companion object {
        fun createFrom(errorFields: List<FieldError>) = BadRequestException(
                errorFields.map { error ->
                    val errorModel = ErrorModel(error.field, error.defaultMessage)
                    errorModel
                })
    }
}