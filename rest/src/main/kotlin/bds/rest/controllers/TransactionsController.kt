package bds.rest.controllers

import bds.rest.Validator
import bds.rest.db.AccountsRepository
import bds.rest.db.TransactionsRepository
import bds.rest.exceptions.BadRequestException
import bds.rest.exceptions.NotFoundException
import bds.rest.models.Transaction
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.validation.BindingResult
import org.springframework.validation.ObjectError
import org.springframework.web.bind.annotation.*
import java.net.URI
import javax.validation.Valid

@RestController
class TransactionsController {

    @Autowired private lateinit var transactionsRepository: TransactionsRepository
    @Autowired private lateinit var accountsRepository: AccountsRepository
    @Autowired private lateinit var validator: Validator

    @PostMapping("/accounts/{id}/history")
    fun postTransaction(@PathVariable("id") accountId: String, @RequestBody @Valid transaction: Transaction, bindingResult: BindingResult): ResponseEntity<Any> {
        if (!accountId.matches(Regex(Transaction.accountNumberRegex))) bindingResult.addError(ObjectError("path_id", "Incorrect account number"))
        if (bindingResult.hasErrors()) throw BadRequestException.createFrom(bindingResult.fieldErrors)
        val account = accountsRepository.findByAccountNumber(accountId) ?: throw NotFoundException()
        validator.validate(account, transaction)
        if (validator.hasErrors()) throw BadRequestException(validator.errorModels())
        transaction.destinationAccount = accountId
        transaction.balance = account.balance - transaction.amount
        val saved = transactionsRepository.save(transaction)
        return ResponseEntity.ok().build()
    }
}