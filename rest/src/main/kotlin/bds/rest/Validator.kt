package bds.rest

import bds.rest.exceptions.BadRequestException
import bds.rest.models.Account
import bds.rest.models.ErrorModel
import bds.rest.models.Transaction
import org.springframework.stereotype.Component

@Component
class Validator {
    private var errorModels: MutableList<ErrorModel> = mutableListOf()

    fun hasErrors(): Boolean {
        return errorModels.isNotEmpty()
    }

    fun errorModels(): List<ErrorModel> {
        return errorModels
    }

    fun validate(account: Account, transaction: Transaction) {
        errorModels.clear()
        if (account.balance - transaction.amount <0) {
            errorModels.add(ErrorModel.create(ErrorModel.ErrorType.NOT_ENOUGH_MONEY))
        }
        if (transaction.destinationAccount.equals(transaction.sourceAccount)) {
            errorModels.add(ErrorModel.create(ErrorModel.ErrorType.DST_SRC))
        }
    }
}