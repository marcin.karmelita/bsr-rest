package bds.rest.models

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlType

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "account", propOrder = arrayOf("id", "balance", "ownerId"))
class Account {
    @XmlElement(name = "owner_id", required = true)
    lateinit var ownerId: String
    @XmlElement(required = true)
    lateinit var id: String
    var balance: Int = 0
    @XmlElement(name="account_number", required = true)
    lateinit var accountNumber: String
}