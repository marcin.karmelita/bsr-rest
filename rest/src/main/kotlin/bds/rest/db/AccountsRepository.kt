package bds.rest.db

import bds.rest.models.Account
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Component

@Component
interface AccountsRepository : MongoRepository<Account, String> {
    fun findById(id: String): Account?
    fun findByAccountNumber(accountNumber: String): Account?
}