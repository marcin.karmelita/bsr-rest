package bds.rest.models

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlType

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "error", propOrder = arrayOf("id", "ownerId"))
class ErrorModel {
    @XmlElement(required = true)
    lateinit var field: String
    @XmlElement(required = true)
    lateinit var error: String



    internal constructor(field: String, error: String) {
        this.error = error
        this.field = field
    }

    enum class ErrorType(val field: String, val error: String) {
        NOT_ENOUGH_MONEY("", "Not enough sources to complete transaction"),
        DST_SRC("", "Source and destination accounts cannot be the same");

        fun error(): ErrorModel {
            return ErrorModel(field, error)
        }
    }

    companion object {
        fun create(errorType: ErrorType) = errorType.error()
    }
}

